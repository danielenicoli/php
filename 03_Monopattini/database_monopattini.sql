create database t02_monopattini;
use t02_monopattini;

create table utenti (
    id int not null primary key auto_increment,
    tipo int default 0,
    cognome varchar(50) not null,
    nome varchar(50) not null,
    email varchar(100) not null,
    password varchar(100) not null
);

create table monopattini (
    id int not null primary key auto_increment,
    batteria int not null default 0,
    stato varchar(50) not null,
    utente int,
    FOREIGN KEY (utente) REFERENCES utenti(id)
);

INSERT INTO utenti (tipo, cognome, nome, email, password) VALUES ('1', 'Rossi', 'Andrea', 'rossi.andrea@comune.terme.it', 'andrea');
INSERT INTO utenti (tipo, cognome, nome, email, password) VALUES ('1', 'Verdi', 'Mattia', 'mattia.verdi@comune.terme.it', 'mattia');
INSERT INTO utenti (tipo, cognome, nome, email, password) VALUES ('0', 'Gialli', 'Sara', 'saragialli@gmail.com', 'sara');
INSERT INTO utenti (tipo, cognome, nome, email, password) VALUES ('0', 'Neri', 'Giulia', 'giulianeri@gmail.com', 'giulia');


INSERT INTO monopattini (batteria, stato) VALUES ('38', 'In manutenzione');
INSERT INTO monopattini (batteria, stato, utente) VALUES ('78', 'Disponibile', '3');
INSERT INTO monopattini (batteria, stato) VALUES ('23', 'In manutenzione');
INSERT INTO monopattini (batteria, stato) VALUES ('98', 'Disponibile');
INSERT INTO monopattini (batteria, stato) VALUES ('55', 'Disponibile');
INSERT INTO monopattini (batteria, stato) VALUES ('58', 'Disponibile');
INSERT INTO monopattini (batteria, stato) VALUES ('87', 'Disponibile');
INSERT INTO monopattini (batteria, stato) VALUES ('10', 'In manutenzione');
INSERT INTO monopattini (batteria, stato) VALUES ('54', 'In manutenzione');

alter table monopattini add posizione varchar(100);