<html>

<head>
    <title>Elenco studenti</title>
</head>

<body>
    <h1>Elenco studenti della classe</h1>

    <?php
    require_once "conn.php"; //https://www.php.net/manual/en/function.require-once.php

    $sql = "SELECT matricola, nome, cognome FROM studenti ORDER BY cognome, nome ASC";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "- " . $row["cognome"] . " "  . $row["nome"] . "<br>";
        }
    } else {
        echo "Nessuno studente";
    }

    $conn->close();
    ?>
</body>

</html>