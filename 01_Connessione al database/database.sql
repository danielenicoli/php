DROP TABLE IF EXISTS 'studenti';
CREATE TABLE 'studenti' (
  'matricola' int NOT NULL AUTO_INCREMENT,
  'nome' varchar(75) NOT NULL,
  'cognome' varchar(75) NOT NULL,
  PRIMARY KEY ('matricola')
) ENGINE=InnoDB;


INSERT INTO 'studenti' VALUES (1,'Sara','Bianchi'),(2,'Andrea','Rossi'),(3,'Alessandro','Gialli'),(4,'Giulia','Verdi');