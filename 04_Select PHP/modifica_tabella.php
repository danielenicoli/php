<?php
if(isset($_GET["id"])) {
    echo "Selezionato prodotto ". $_GET["id"] . " tramite GET<br><br>";
    require_once "conn.php";

    $sql = "SELECT * FROM prodotti WHERE id=".$_GET["id"];
    //echo $sql;

    $result = $conn->query($sql);
    if ($result->num_rows >0) {
        echo "<form method='post' action='script_modifica.php'>";
        echo "<table><th>Nome prodotto</th><th>Prezzo</th><th>Reparto</th>";
        while($row = $result->fetch_assoc()) {
            $reparto_prodotto_selezionato = $row["reparto"];

            echo "<tr>";
         
            echo "<td><input type='text' name='nome' value='".$row["nome"]."'/></td>";
            echo "<td><input type='text' name='prezzo' value='".$row["prezzo"]."'/></td>";

            //GESTISCO LA LISTBOX
            $sql = "SELECT * FROM reparti";
            $result = $conn->query($sql);
            if ($result->num_rows >0) {
                echo "<td><select name='reparto'>";
                while($row = $result->fetch_assoc()) {
                    $descrizione_reparto_select = $row["descrizione"];

                    //CONTROLLO SE IL REPARTO DEL PRODOTTO SELEZIONATO È UGUALE AL REPARTO DELLA OPTION CORRENTE
                    if($descrizione_reparto_select == $reparto_prodotto_selezionato) {
                        echo "<option value='....' selected>".$row["descrizione"]."</option>";
                    }
                    else {
                        echo "<option value='....' >".$row["descrizione"]."</option>";
                    }
                }
                echo "</select></td></tr></table>";
            }
        }
        echo "</form>";
    }
    $conn->close();
}
else {
    echo "Nessun prodotto selezionato";
}
?>
